﻿using FluentResults;
using ImageOrganizer.ConsoleApp;
using ImageOrganizer.Core;

var exifTool = new ExifTool();

if (args.Length < 2)
{
    Console.WriteLine(@"
NAME
    Image Organizer

SYNOPSIS
    imgorg <file> <command> [keywords]

COMMANDS
    --add <keywords>
        Add specified keywords to the file.

    --get
        Retrieve keywords from the file.

    --remove <keywords>
        Remove specified keywords from the file.

    --find <keywords>
        Find files with specified keywords and open in a temp directory as links

    --find-recursive <keywords>
        Find files recursively with specified keywords and open in a temp directory as links

    --find-print <keywords>
        Find and print URIs of files with specified keywords.

    --find-print-recursive <keywwords>
        Find and print URIs of files recursively with specified keywords.

    --keywords
        Get a list of all unique keywords

    --keywords-recursive
        Get a list of all unique keywords including files in sub-directories

    --test [test_files]
        Special Test method that ensures all features are working on a set of provided test files

DESCRIPTION
    This tool allows you to add, get, remove, and find keywords in the EXIF data of a specified file.

EXAMPLES
    Add keywords:
        imgorg image.jpg --add keyword1 keyword2

    Get keywords:
        imgorg image.jpg --get

    Remove keywords:
        imgorg image.jpg --remove keyword1

    Find files by keywords:
        imgorg . --find keyword1 keyword2

    Find and print URIs:
        imgorg . --find-print keyword1 keyword2

    Test the functionality on test files:
        imgorg --test ./file1.jpg ./file2.png

");
    return;
}

if (args[0] == "--test")
{
    var testFiles = args.Skip(1).ToList();

    foreach (var file in testFiles)
    {
        var initialKeywords = exifTool.GetKeywords(file);
        if (initialKeywords.IsFailed)
        {
            Console.WriteLine($"Failed to read initial state: {string.Join(", ", initialKeywords.Errors.Select(e => e.Message))}");
            Console.WriteLine("Aborted");
            continue;
        }

        AssertResult(exifTool.RemoveKeywords(file, initialKeywords.Value), "Initial Keyword Clear");
        Console.WriteLine($"Testing {file}");
        AssertKeywords(exifTool.GetKeywords(file), []);
        AssertResult(exifTool.AddKeywords(file, ["test1", "test2"]), "Initial Add");
        AssertKeywords(exifTool.GetKeywords(file), ["test1", "test2"]);
        AssertResult(exifTool.AddKeywords(file, ["test1"]), "Duplicate Add");
        AssertKeywords(exifTool.GetKeywords(file), ["test1", "test2"]);
        AssertResult(exifTool.AddKeywords(file, ["test3"]), "Standard Add");
        AssertKeywords(exifTool.GetKeywords(file), ["test1", "test2", "test3"]);
        AssertResult(exifTool.RemoveKeywords(file, ["test2", "test3"]), "Standard Remove");
        AssertKeywords(exifTool.GetKeywords(file), ["test1"]);
        AssertResult(exifTool.RemoveKeywords(file, ["test1", "SOMETHING_ELSE"]), "Full Remove With Extra");
        AssertKeywords(exifTool.GetKeywords(file), []);
    }

    return;
}

switch (args[1])
{
    case "--add":
        PrintResult(exifTool.AddKeywords(args[0], args.Skip(2)));
        break;
    case "--get":
        PrintResultStrings(exifTool.GetKeywords(args[0]));
        break;
    case "--remove":
        PrintResult(exifTool.RemoveKeywords(args[0], args.Skip(2)));
        break;
    case "--find-recursive":
        OpenUriStringsResult(exifTool.FindByKeywords(args[0], args.Skip(2), true));
        break;
    case "--find":
        OpenUriStringsResult(exifTool.FindByKeywords(args[0], args.Skip(2)));
        break;
    case "--find-print-recursive":
        PrintUriStringsResult(exifTool.FindByKeywords(args[0], args.Skip(2), true));
        break;
    case "--find-print":
        PrintUriStringsResult(exifTool.FindByKeywords(args[0], args.Skip(2)));
        break;
    case "--keywords-recursive":
        PrintResultStrings(exifTool.GetAllKeywords(args[0], true));
        break;
    case "--keywords":
        PrintResultStrings(exifTool.GetAllKeywords(args[0], false));
        break;
    default:
        Console.WriteLine($"Invalid argument '{args[1]}'");
        break;
}

void PrintResult(Result r)
{
    if (r.IsSuccess)
    {
        Console.WriteLine("SUCCESS");
    }
    else
    {
        Console.WriteLine($"FAILED: {string.Join(", ", r.Errors.Select(e => e.Message))}");
    }
}

void PrintResultStrings(Result<IReadOnlyCollection<string>> r)
{
    if (r.IsSuccess)
    {
        Console.WriteLine(string.Join(", ", r.Value));
    }
    else
    {
        Console.WriteLine($"FAILED: {string.Join(", ", r.Errors.Select(e => e.Message))}");
    }
}

void PrintUriStringsResult(Result<IReadOnlyCollection<string>> r)
{
    if (r.IsSuccess)
    {
        Console.WriteLine(string.Join("\n", r.Value));
    }
    else
    {
        Console.WriteLine(string.Join("\n", r.Value.Select(file =>
        {
            string uri = new Uri(file).AbsoluteUri;
            return $"\u001b]8;;{uri}\u0007{file}\u001b]8;;\u0007";
        })));
    }
}

void OpenUriStringsResult(Result<IReadOnlyCollection<string>> r)
{
    if (r.IsFailed)
    {
        Console.WriteLine($"FAILED: {string.Join(", ", r.Errors.Select(e => e.Message))}");
    }
    else
    {
        if (r.Value.Count == 0)
        {
            Console.WriteLine("Nothing Found");
        }
        else
        {
            ShortcutCreator.CreateShortcutsAndOpen(r.Value);
        }
    }
}

void AssertKeywords(Result<IReadOnlyCollection<string>> keywordsResult, IEnumerable<string> expected)
{
    if (keywordsResult.IsFailed)
    {
        Console.WriteLine($"FAIL! Reasons: {string.Join(", ", keywordsResult.Errors.Select(e => e.Message))}");
        return;
    }

    var keywords = keywordsResult.Value;

    var a = string.Join(",", keywords);
    var b = string.Join(",", expected);

    if (a != b)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine($"FAIL! '{a}' != '{b}'");
    }
    else
    {
        Console.ForegroundColor= ConsoleColor.Green;
        Console.WriteLine($"SUCCESS! '{a}' != '{b}'");
    }

    Console.ResetColor();
}

void AssertResult(Result result, string label = "unnamed")
{
    if (result.IsFailed)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine($"Operation Failed! Reasons: {string.Join(", ", result.Errors.Select(e => e.Message))}");
    }
    else
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine($"Operation Succeeded! ({label})");
    }

    Console.ResetColor();
}
