﻿using Microsoft.AspNetCore.Components.WebView.Maui;
using Microsoft.Extensions.FileProviders;

namespace ImageOrganizer.MauiHybrid;

public class CustomFilesBlazorWebView : BlazorWebView
{
    public override IFileProvider CreateFileProvider(string contentRootDir)
    {
        var physicalFileProviders = new List<IFileProvider>();

        foreach (var drive in DriveInfo.GetDrives())
        {
            if (drive.IsReady)
            {
                physicalFileProviders.Add(new PhysicalFileProvider(drive.RootDirectory.FullName));
            }
        }

        physicalFileProviders.Add(base.CreateFileProvider(contentRootDir));
        return new CompositeFileProvider(physicalFileProviders);
    }
}
