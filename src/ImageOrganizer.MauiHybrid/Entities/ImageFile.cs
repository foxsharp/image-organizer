﻿namespace ImageOrganizer.MauiHybrid.Entities;

public class ImageFile(string filePath, string mimeType)
{
    public string FilePath { get; } = filePath;

    public string MimeType { get; } = mimeType;

    public string SrcPath => FilePath.Length > 3 ? FilePath[3..] : string.Empty;

    public string DataUri
    {
        get
        {
            byte[] imageBytes = File.ReadAllBytes(FilePath);
            string base64 = Convert.ToBase64String(imageBytes);

            return $"data:image/webp;base64,{base64}";
        }
    }
}
