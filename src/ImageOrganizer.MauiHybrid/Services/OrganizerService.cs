﻿using HeyRed.Mime;
using ImageOrganizer.MauiHybrid.Entities;

namespace ImageOrganizer.MauiHybrid.Services;

public class OrganizerService : IOrganizerService
{
    public ImageFile CreateImageFileFromPath(string path)
    {
        var mimeType = MimeTypesMap.GetMimeType(path);

        return new(path, mimeType);
    }
}
