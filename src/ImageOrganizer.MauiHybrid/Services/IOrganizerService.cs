﻿using ImageOrganizer.MauiHybrid.Entities;

namespace ImageOrganizer.MauiHybrid.Services;

public interface IOrganizerService
{
    ImageFile CreateImageFileFromPath(string path);
}
