﻿using ImageOrganizer.Core;
using ImageOrganizer.MauiHybrid.Entities;
using ImageOrganizer.MauiHybrid.Services;
using Microsoft.AspNetCore.Components;

namespace ImageOrganizer.MauiHybrid.Components.Pages;

public partial class Home
{
    [Inject]
    public IOrganizerService OrganizerService { get; init; } = default!;

    [Inject]
    private ExifTool ExifTool { get; init; } = default!;
    
    private ImageFile[] _imageFiles = [];
    private ImageFile? _selectedFile = null;

    private string _pathInput = string.Empty;
    private bool _recursive = false;
    private string _errorMessage = string.Empty;

    private string[] _availableTags = [];
    private string[] _currentKeywords = [];
    private string _customKeywordInput = string.Empty;
    private string _selectedDropdownExistingKeyword = string.Empty;
    private string _selectedDropdownAvailableKeyword = string.Empty;
    private string _keywordsFilter = string.Empty;

    public void LoadDirectory()
    {
        if (!Directory.Exists(_pathInput))
        {
            _errorMessage = $"Path not found: '{_pathInput}'";
            return;
        }

        var keywordsFilter = 
            string.IsNullOrWhiteSpace(_keywordsFilter)
            ? []
            : _keywordsFilter.Split(',', StringSplitOptions.RemoveEmptyEntries);

        var result = ExifTool.FindByKeywords(_pathInput, keywordsFilter, _recursive);
        if (result.IsSuccess)
        {
            _imageFiles = result.Value.Select(OrganizerService.CreateImageFileFromPath).ToArray();
        }
        else
        {
            _imageFiles = [];
            _errorMessage = string.Join(", ", result.Errors.Select(e => e.Message));
        }
    }

    public void OnAddCustom()
    {
        if (_selectedFile is null || string.IsNullOrEmpty(_customKeywordInput))
        {
            return;
        }

        var result = ExifTool.AddKeywords(_selectedFile.FilePath, [_customKeywordInput]);

        if (result.IsFailed)
        {
            throw new InvalidOperationException("Failed to add a keyword");
        }

        _customKeywordInput = string.Empty;
        FetchCurrentKeywords();
        FetchAllKeywords();
    }

    private void SelectImageFile(ImageFile file)
    {
        _selectedFile = file;
        FetchCurrentKeywords();
        FetchAllKeywords();
    }

    private void FetchCurrentKeywords()
    {
        if (_selectedFile is null)
        {
            return;
        }

        var keywordsResult = ExifTool.GetKeywords(_selectedFile.FilePath);
        if (keywordsResult.IsSuccess)
        {
            _currentKeywords = [.. keywordsResult.Value];
        }
    }

    private void FetchAllKeywords()
    {
        var tagsResult = ExifTool.GetAllKeywords(_pathInput, _recursive);
        if (tagsResult.IsFailed)
        {
            throw new InvalidOperationException("Failed to fetch available keywords");
        }

        _availableTags = [.. tagsResult.Value];
    }

    private void DeselectItem()
    {
        _selectedFile = null;
    }

    private void RemoveSelectedKeyword()
    {
        if (_selectedFile is null)
        {
            return;
        }

        var result = ExifTool.RemoveKeywords(_selectedFile.FilePath, [_selectedDropdownExistingKeyword]);
        if (result.IsFailed)
        {
            _errorMessage = string.Join(", ", result.Errors.Select(e => e.Message));
            return;
        }

        _selectedDropdownExistingKeyword = string.Empty;
        FetchCurrentKeywords();
        FetchAllKeywords();
    }

    private void AddSelectedKeyword()
    {
        if (_selectedFile is null)
        {
            return;
        }

        var result = ExifTool.AddKeywords(_selectedFile.FilePath, [_selectedDropdownAvailableKeyword]);
        if (result.IsFailed)
        {
            _errorMessage = string.Join(", ", result.Errors.Select(e => e.Message));
            return;
        }

        _selectedDropdownExistingKeyword = string.Empty;
        FetchCurrentKeywords();
        FetchAllKeywords();
    }
}
