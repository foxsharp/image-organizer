﻿using ImageOrganizer.MauiHybrid.Entities;
using Microsoft.AspNetCore.Components;

namespace ImageOrganizer.MauiHybrid.Components.Common;

public partial class ImageFileGrid
{
    [Parameter]
    [EditorRequired]
    public ImageFile[] Value { get; set; } = [];

    [Parameter]
    public int PageSize { get; set; } = 5;

    [Parameter]
    public EventCallback<ImageFile> OnSelected { get; set; }

    private int PageCount => Value.Length / PageSize;

    private bool NextPageAvailable => _currentPage < PageCount - 1;
    private bool PrevPageAvailable => _currentPage > 0;

    private string NextPageClass => NextPageAvailable ? "" : "disabled";
    private string PrevPageClass => PrevPageAvailable ? "" : "disabled";

    private int _currentPage = 0;

    private void OnPrevPageClicked()
    {
        if (PrevPageAvailable)
        {
            _currentPage--;
        }
    }

    private void OnNextPageClicked()
    {
        if (NextPageAvailable)
        {
            _currentPage++;
        }
    }

    private async Task ImageClickedAsync(ImageFile file)
    {
        await OnSelected.InvokeAsync(file);
    }
}
