﻿using ImageOrganizer.Core;
using ImageOrganizer.MauiHybrid.Services;

namespace ImageOrganizer.MauiHybrid;

public static class ServiceCollectionExtensions
{
    public static void AddOrganizer(this IServiceCollection services)
    {
        services.AddSingleton<ExifTool>();
        services.AddTransient<IOrganizerService, OrganizerService>();
    }
}
