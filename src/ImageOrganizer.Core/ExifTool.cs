﻿using FluentResults;
using System.Diagnostics;

namespace ImageOrganizer.Core;

public class ExifTool
{
    const string KeywordsTag = "XMP:Keywords";
    const string Overwrite = "-overwrite_original";

    public Result<IReadOnlyCollection<string>> GetKeywords(string file)
    {
        var processResult = CreateExifToolProcess($"-{KeywordsTag} \"{file}\"");
        if (processResult.IsFailed)
        {
            return Result.Fail(processResult.Errors);
        }
        var process = processResult.Value;
        process.Start();

        var output = process.StandardOutput.ReadToEnd();
        var error = process.StandardError.ReadToEnd();

        process.WaitForExit();

        if (process.ExitCode != 0)
            return Result.Fail(error);

        if (string.IsNullOrWhiteSpace(output))
            return Result.Ok((IReadOnlyCollection<string>)[]);

        var split = output.Split(':');
        if (split.Length != 2)
            return Result.Fail($"Failed to split by ':' - Exiftool Output:\n{output}");

        return Result.Ok((IReadOnlyCollection<string>)split[1].Split(",").Select(keyword => keyword.Trim()).ToList());
    }

    public Result AddKeywords(string file, IEnumerable<string> keywords)
    {
        if (!keywords.Any())
            return Result.Ok();

        keywords = keywords.Select(k => k.ToLower()).ToList();

        var currentKeywordsResult = GetKeywords(file);

        if (currentKeywordsResult.IsFailed)
            return Result.Fail("Failed to read current keywords. Write aborted.");

        var parameters = string.Join(" ", $"{Overwrite} -{KeywordsTag}=\"{string.Join(",", currentKeywordsResult.Value.Union(keywords, StringComparer.OrdinalIgnoreCase))}\"");

        var processResult = CreateExifToolProcess($"{parameters} \"{file}\"");
        if (processResult.IsFailed)
        {
            return Result.Fail(processResult.Errors);
        }
        var process = processResult.Value;
        process.Start();

        var output = process.StandardOutput.ReadToEnd();
        var error = process.StandardError.ReadToEnd();

        process.WaitForExit();

        if (process.ExitCode != 0)
            return Result.Fail(error);

        if (!output.Contains("image files updated"))
            return Result.Fail($"Adding keywords failed. Output:\n{output}");

        return Result.Ok();
    }

    public Result RemoveKeywords(string file, IEnumerable<string> keywords)
    {
        if (!keywords.Any())
            return Result.Ok();

        var currentKeywordsResult = GetKeywords(file);

        if (currentKeywordsResult.IsFailed)
            return Result.Fail("Failed to read current keywords. Write aborted.");

        var currentKeywords = currentKeywordsResult.Value.ToList();

        if (keywords.All(k => !currentKeywords.Contains(k, StringComparer.OrdinalIgnoreCase)))
        {
            return Result.Fail("None of the provided keywords matched existing ones");
        }

        var parameters = $"{Overwrite} -{KeywordsTag}=\"{string.Join(",", currentKeywords.Except(keywords, StringComparer.OrdinalIgnoreCase).Select(k => k.ToLower()))}\"";

        var processResult = CreateExifToolProcess($"{parameters} \"{file}\"");
        if (processResult.IsFailed)
        {
            return Result.Fail(processResult.Errors);
        }
        var process = processResult.Value;
        process.Start();

        var output = process.StandardOutput.ReadToEnd();
        var error = process.StandardError.ReadToEnd();

        process.WaitForExit();

        if (process.ExitCode != 0)
            return Result.Fail(error);

        if (!output.Contains("image files updated"))
            return Result.Fail($"Removing keywords failed. Output:\n{output}");

        return Result.Ok();
    }

    public Result<IReadOnlyCollection<string>> FindByKeywords(string directory, IEnumerable<string> keywords, bool recursive = false)
    {
        directory = Path.GetFullPath(directory.TrimEnd('\\').TrimEnd('/'));

        if (!Directory.Exists(directory))
        {
            return Result.Fail($"Directory Not Found: {directory}");
        }

        var argumentsList = new List<string> { "-filename", $"\"{directory}\"" };

        if (!keywords.Any())
        {
            argumentsList = argumentsList
                                .Prepend("\"!$Keywords\"")
                                .Prepend("-if")
                                .ToList();
        }
        else
        {
            var searchPattern = string.Join('|', keywords);
            argumentsList.Insert(2, $"-if \"$Keywords=~/{searchPattern.ToLower()}/\"");
        }

        if (recursive)
        {
            argumentsList = argumentsList.Prepend("-r").ToList();
        }

        var processResult = CreateExifToolProcess(string.Join(" ", argumentsList));
        if (processResult.IsFailed)
        {
            return Result.Fail(processResult.Errors);
        }
        var process = processResult.Value;
        process.Start();

        var output = process.StandardOutput.ReadToEnd();
        var error = process.StandardError.ReadToEnd();

        process.WaitForExit();

        if (process.ExitCode == 2)
            return Result.Fail($"Nothing Found - Details: {output}");

        if (process.ExitCode != 0)
            return Result.Fail(error);

        if (!output.Contains("========"))
        {
            return Result.Ok<IReadOnlyCollection<string>>([]);
        }

        try
        {
            var result = output.Split('\n').Where(l => l.StartsWith("========")).Select(l =>
            {
                var split = l.Split("= ");
                if (split.Length != 2)
                    throw new InvalidOperationException($"Failed to split line: '{l}'");

                if (recursive)
                {
                    return split[1].Trim();
                }

                return Path.Combine(directory, split[1].Trim());
            }).ToList();

            return result;
        }
        catch (Exception e)
        {
            return Result.Fail(e.Message);
        }
    }

    public Result<IReadOnlyCollection<string>> GetAllKeywords(string directory, bool recursive = false)
    {
        directory = Path.GetFullPath(directory.TrimEnd('\\').TrimEnd('/'));

        if (!Directory.Exists(directory))
        {
            return Result.Fail($"Directory Not Found: {directory}");
        }

        var argumentsList = new List<string> { $"\"{directory}\"", "-Keywords", "-if", "\"$Keywords\"" };

        if (recursive)
        {
            argumentsList.Insert(1, "-r");
        }

        var processResult = CreateExifToolProcess(string.Join(' ', argumentsList));
        if (processResult.IsFailed)
        {
            return Result.Fail(processResult.Errors);
        }

        var process = processResult.Value;
        process.Start();

        var output = process.StandardOutput.ReadToEnd();
        var error = process.StandardError.ReadToEnd();

        process.WaitForExit();

        if (process.ExitCode != 0)
            return Result.Fail(error);

        if (!output.Contains("Keywords"))
        {
            return Result.Ok<IReadOnlyCollection<string>>([]);
        }

        var uniqueKeywords = output.Split('\n')
            .Where(l => l.StartsWith("Keywords"))
            .SelectMany(l =>
            {
                var split = l.Split(':');
                if (split.Length != 2)
                    throw new ArgumentException($"Failed to split the line: '{l}'");

                string rawKeywords = split[1];

                return rawKeywords
                    .Split(',', StringSplitOptions.RemoveEmptyEntries)
                    .Select(keyword => keyword.Trim());
            })
            .Distinct(StringComparer.OrdinalIgnoreCase)
            .ToList();

        return uniqueKeywords;
    }

    private static Result<Process> CreateExifToolProcess(string arguments)
    {
        return Result.Try<Process>(() => new()
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = "exiftool",
                Arguments = arguments,
                WindowStyle = ProcessWindowStyle.Hidden,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            }
        }, exception => new Error($"Failed to run ExifTool, please make sure it is installed an accessable throug the PATH variable"));
    }
}
