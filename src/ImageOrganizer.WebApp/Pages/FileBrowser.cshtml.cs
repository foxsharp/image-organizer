using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ImageOrganizer.WebApp.Pages;

public class FileBrowserModel : PageModel
{
    public string Path { get; private set; } = string.Empty;

    public string[] SubDirectories { get; private set; } = [];

    public string[] Files { get; private set; } = [];

    public bool HasParentDir { get; private set; } = false;

    public static bool IsImageFile(string filePath) =>
        new[] { ".jpg", ".jpeg", ".png", ".gif", ".bmp", ".tiff", ".webp" }
        .Contains(System.IO.Path.GetExtension(filePath)?.ToLower());

    public void OnGet(string? path)
    {
        if (!Directory.Exists(path))
            return;

        Path = path;

        var dirInfo = new DirectoryInfo(Path);
        SubDirectories = dirInfo.GetDirectories().Select(directory => directory.FullName).ToArray();
        Files = dirInfo.GetFiles().Select(file => file.FullName).ToArray();

        HasParentDir = dirInfo.Parent is not null;
    }
}
