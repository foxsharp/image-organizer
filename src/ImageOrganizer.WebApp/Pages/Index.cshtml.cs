using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ImageOrganizer.WebApp.Pages
{
    public class IndexModel : PageModel
    {
        public string[] DriveLetters { get; private set; } = [];

        public void OnGet()
        {
            var drives = DriveInfo.GetDrives();

            if (drives is not null)
            {
                DriveLetters = drives
                    .Where(drive => drive.IsReady)
                    .Select(drive => drive.Name)
                    .ToArray();
            }
        }
    }
}
