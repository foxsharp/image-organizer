﻿using Microsoft.AspNetCore.Mvc;

namespace ImageOrganizer.WebApp.Controllers;

[Route("api/[controller]")]
[ApiController]
public class FilesController : ControllerBase
{
    [HttpPost("search")]
    public string[] Search(string path, string query, bool recursive)
    {
        var files = Directory.GetFiles(path, query, recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
            
        return files;
    }

    [HttpGet("image")]
    public async Task<IActionResult> Image(string path)
    {
        var fileBytes = await System.IO.File.ReadAllBytesAsync(path);

        return File(fileBytes, "application/octet-stream");
    }
}
