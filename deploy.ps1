Remove-Item -Recurse -Force .\output
dotnet publish ./src/ImageOrganizer.ConsoleApp/ImageOrganizer.ConsoleApp.csproj -c Release -r win-x64 --self-contained false -p:PublishSingleFile=true -p:IncludeAllContentForSelfExtract=true -o output
Remove-Item -Path .\output\*.pdb -Force
Move-Item -Path .\output\ImageOrganizer.ConsoleApp.exe -Destination C:\bin\imgorg.exe -Force
